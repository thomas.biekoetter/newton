program main

use func, only : f, N
use newton, only : new_complex_multi

implicit none

integer, parameter :: dp = selected_real_kind(15,307)

complex(dp), dimension(:), allocatable :: seed ! Initial x-value
complex(dp), dimension(:), allocatable :: res ! Root
complex(dp), dimension(:), allocatable :: ur ! f-value at seed
complex(dp), dimension(:), allocatable :: check ! f-value at root

allocate(seed(N))
allocate(res(N))
allocate(ur(N))
allocate(check(N))

seed = (1.0E0_dp, 1.0E0_dp)

call f(seed, ur)
call new_complex_multi(f, N, seed, res)
call f(res, check)

write(*,*) "f(seed) = ", ur
write(*,*)
write(*,*) "f has root at: ", res
write(*,*)
write(*,*) "f(root) = ", check

end program main

module func

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

integer, parameter :: N = 5

complex(dp) :: a = (1.0E0_dp, 1.0E0_dp)
complex(dp) :: b = (2.0E0_dp, 0.0E0_dp)
complex(dp) :: c = (0.0E0_dp, 3.0E0_dp)

contains


subroutine f(x, y)

  complex(dp), intent(in) :: x(N)
  complex(dp), intent(out) :: y(N)

  y(1) = x(1)*x(3)**2 - x(3)**2 + a + x(5)
  y(2) = x(4) + x(1) + a*b + x(4)*x(2)+ x(5)
  y(3) = x(4) - x(2) - 2*a+ x(5)
  y(4) = x(3)**3 - x(1)**2 - c+ x(5)
  y(5) = exp(x(5))

  y = y + sum(x)**2

end subroutine f


end module func

module newton

use gaussjordan, only : eliminate

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

contains


subroutine new_complex_multi(f, N, x, y)

  external :: f
  integer, intent(in) :: N
  complex(dp), intent(in) :: x(N)
  complex(dp), intent(out) :: y(N)

  real(dp) :: xm(2*N), xn(2*N)
  real(dp) :: fm(2*N), dfm(2*N,2*N), fn(2*N)
  real(dp) :: step(2*N)
  real(dp) :: eps = 1.0E-1_dp
  real(dp) :: prec = 1.0E-10_dp
  logical :: goon
  integer :: gooncount
  integer :: i
  integer :: Nr

  Nr = 2*N

  goon = .true.
  xm(1:N) = real(x)
  xm(N+1:Nr) = aimag(x)
  call func(f, Nr, xm, fm)

  do i=1,Nr
    if (isnan(fm(i))) then
      write(*,*)  'Seed evaluates to NaN'
      call exit
    else if ((1.0E0_dp / fm(i)).eq.0.0E0_dp) then
      write(*,*)  'Seed evaluates to infinity'
      call exit
    endif
  enddo

  do while (goon)
    gooncount = 0
    call calcjacob(f, Nr, xm, dfm)
    call eliminate(Nr, dfm, -fm, step)
    xn = xm + eps * step
    call func(f, Nr, xn, fn)
    do i=1,Nr
      if (isnan(fn(i))) then
        write(*,*) "Bad seed or ill-defined, Gauss-Jordan returned NaNs."
        call exit
      endif
      if ((1 / fn(i)).eq.0.0E0_dp) then
        write(*,*) "Bad seed or ill-defined, jumped far away and function returned infinity."
        call exit
      endif
      if (abs(fn(i)).lt.prec) then
        gooncount = gooncount + 1
      endif
    enddo
    if (gooncount.eq.Nr) then
      goon = .false.
    else
      xm = xn
      fm = fn
    endif
  enddo
  do i=1,N
    y(i) = (1.0E0_dp, 0.0E0_dp) * xn(i) +  &
      (0.0E0_dp, 1.0E0_dp) * xn(N + i)
  enddo

end subroutine new_complex_multi


subroutine func(f, N, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  complex(dp) :: xc(N / 2)
  complex(dp) :: yc(N / 2)
  integer :: Nc
  integer :: i

  Nc = N / 2

  do i=1,Nc
    xc(i) = (1.0E0_dp, 0.0E0_dp) * x(i) +  &
      (0.0E0_dp, 1.0E0_dp) * x(Nc + i)
  enddo

  call f(xc, yc)

  y(1:Nc) = real(yc)
  y(Nc+1:N) = aimag(yc)

end subroutine func


subroutine calcjacob(f, N, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N,N)

  integer :: i

  do i=1,N
    call deriv(f, N, i, x, y(:, i))
  enddo

end subroutine calcjacob


subroutine deriv(f, N, i, x, y)

  external :: f
  integer, intent(in) :: N, i
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: eps = 1.0E-8_dp
  real(dp) :: x1(N), x2(N), y1(N), y2(N)

  x1 = x
  x1(i) = x1(i) - eps
  x2 = x
  x2(i) = x2(i) + eps
  call func(f, N, x1, y1)
  call func(f, N, x2, y2)

  y = (y2 - y1) / (2.0E0_dp * eps)

end subroutine deriv


end module newton

program main

use func, only : f, N
use newton, only : new_real_multi

implicit none

integer, parameter :: dp = selected_real_kind(15,307)

real(dp), dimension(:), allocatable :: seed ! Initial x-value
real(dp), dimension(:), allocatable :: res ! Root
real(dp), dimension(:), allocatable :: check ! f-value at root

allocate(seed(N))
allocate(res(N))
allocate(check(N))

seed(1) = 1
seed(2) = 1

call new_real_multi(f, N, seed, res)
call f(res, check)
write(*,*) "f has root at: ", res
write(*,*) "f(root) = ", check

end program main

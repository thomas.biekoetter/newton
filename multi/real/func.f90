module func

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

integer, parameter :: N = 6

contains


subroutine f(x, y)

  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  y(1) = x(1) + x(2) + x(4) - 0.001
  y(2) = x(5) + x(6) - 55
  y(3) = x(1) + x(2) + x(3) + 2*x(5) + x(6) - 110.001
  y(4) = x(1) - 0.1*x(2)
  y(5) = x(1) - 1.0E4_dp*x(3)*x(4)
  y(6) = x(5) - 55.0E14_dp*x(3)*x(6)

end subroutine f


end module func

module newton

use gaussjordan, only : eliminate

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

contains


subroutine new_real_multi(f, N, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: xm(N), xn(N)
  real(dp) :: fm(N), dfm(N,N), fn(N)
  real(dp) :: step(N)
  real(dp) :: eps = 1.0E-1_dp
  real(dp) :: prec = 1.0E-10_dp
  logical :: goon
  integer :: gooncount
  integer :: i

  goon = .true.
  xm = x
  call f(xm, fm)

  do i=1,N
    if (isnan(fm(i))) then
      write(*,*)  'Seed evaluates to NaN'
      call exit
    else if ((1.0E0_dp / fm(i)).eq.0.0E0_dp) then
      write(*,*)  'Seed evaluates to infinity'
      call exit
    endif
  enddo

  do while (goon)
    gooncount = 0
    call calcjacob(f, N, xm, dfm)
    call eliminate(N, dfm, -fm, step)
    xn = xm + eps * step
    call f(xn, fn)
    do i=1,N
      if (isnan(fn(i))) then
        write(*,*) "Bad seed, Gauss-Jordan returned NaNs."
        call exit
      endif
      if ((1 / fn(i)).eq.0.0E0_dp) then
        write(*,*) "Jumped far away and function returned infinity."
        call exit
      endif
      if (abs(fn(i)).lt.prec) then
        gooncount = gooncount + 1
      endif
    enddo
    if (gooncount.eq.N) then
      goon = .false.
    else
      xm = xn
      fm = fn
    endif
  enddo
  y = xn

end subroutine new_real_multi


subroutine calcjacob(f, N, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N,N)

  integer :: i

  do i=1,N
    call deriv(f, N, i, x, y(:, i))
  enddo

end subroutine calcjacob


subroutine deriv(f, N, i, x, y)

  external :: f
  integer, intent(in) :: N, i
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: eps = 1.0E-8_dp
  real(dp) :: x1(N), x2(N), y1(N), y2(N)

  x1 = x
  x1(i) = x1(i) - eps
  x2 = x
  x2(i) = x2(i) + eps
  call f(x1, y1)
  call f(x2, y2)

  y = (y2 - y1) / (2.0E0_dp * eps)

end subroutine deriv


end module newton

# newton

A collection of routines to find roots of
an equation or system of equations in real
or complex variables.

* **single/real/** Contains a method to
find a root of a real function with
a single variable.
* **single/complex/** Contains a method to
find a root of a complex function with
a single variable.
* **multi/real/** Contains a method to
find a root of a system of N real functions with
N variables.
* **multi/complex/** Contains a method to
find a root of a system of N complex functions with
N variables.

In order to use `newton`, the user has
to specify the function(s) in the
module `func.f90`, along with the
number of functions/variables N in
case of a multivariate function.
Finally, in the main program (`main.f90`)
the seed has to be set. The program
is compiled and executed with
```
make all
./runner
```
from within the corresponding directory.

More detailed instructions can be found
in the [documentation].

## About

This software is written by [Thomas Biekötter],
and it is distributed under the General Public
License v.3.

[documentation]: https://www.desy.de/~biek/newtondocu/site
[Thomas Biekötter]: mailto:thomas.biekoetter@desy.de

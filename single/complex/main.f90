program main

use func, only : f
use newton, only : new_complex_single

implicit none

integer, parameter :: dp = selected_real_kind(15,307)

complex(dp) :: seed = (10.0E0_dp, 10.0E0_dp) ! Initial x-value
complex(dp) :: res ! Root
complex(dp) :: check ! f-value at root

call new_complex_single(f, seed, res)
call f(res, check)
write(*,*) "f has root at: ", res
write(*,*) "f(root) = ", check

end program main

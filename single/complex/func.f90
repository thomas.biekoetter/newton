module func

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

complex(dp) :: a = (-1.0E0_dp, 0.5E0_dp)
complex(dp) :: b = (-1.0E1_dp, 0.0E0_dp)

contains


subroutine f(z, y)

  complex(dp), intent(in) :: z
  complex(dp), intent(out) :: y

  y = z**3 + a*z**2 - b*z + b

end subroutine f


end module func

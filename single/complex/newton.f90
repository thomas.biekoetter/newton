module newton

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

contains


subroutine new_complex_single(f, z, y)

  external :: f
  complex(dp), intent(in) :: z
  complex(dp), intent(out) :: y

  complex(dp) :: zm, zn
  complex(dp) :: fm, fn
  real(dp) :: u, v, ux, vx, uy, vy
  real(dp) :: d
  complex(dp) :: step
  real(dp) :: eps = 1.0E-3_dp
  real(dp) :: prec = 1.0E-10_dp
  logical :: goon

  goon = .true.
  zm = z
  call f(zm, fm)

  if ((isnan(real(fm))).or.(isnan(aimag(fm)))) then
    write(*,*)  'Seed evaluates to NaN'
    call exit
  else if (((1 / real(fm)).eq.0.0E0_dp).or.  &
      ((1 / aimag(fm)).eq.0.0E0_dp)) then
    write(*,*)  'Seed evaluates to infinity'
    call exit
  endif

  do while (goon)
    u = real(fm)
    v = aimag(fm)
    call calcdx(f, zm, ux, vx)
    call calcdy(f, zm, uy, vy)
    d = ux**2 + vx**2
    step = (1.0E0_dp, 0.0E0_dp) * (u * ux + v * vx) / d +  &
      (0.0E0_dp, 1.0E0_dp) * (u * uy + v * vy) / d
    zn = zm - eps * step
    call f(zn, fn)
      if (((1 / real(fn)).eq.0.0E0_dp).or.  &
          ((1 / real(fn)).eq.0.0E0_dp)) then
        write(*,*) "Jumped far away and function returned infinity."
        call exit
    endif
    if ((abs(real(fn)).lt.prec).and.(abs(aimag(fn)).lt.prec)) then
      goon = .false.
    else
      zm = zn
      fm = fn
    endif
  enddo
  y = zn

end subroutine new_complex_single


subroutine calcdx(f, z, ux, vx)

  external :: f
  complex(dp), intent(in) :: z
  real(dp), intent(out) :: ux, vx

  complex(dp) :: z1, z2, y1, y2
  real(dp) :: eps = 1.0E-8_dp

  z1 = z - eps * (1.0E0_dp, 0.0E0_dp)
  z2 = z + eps * (1.0E0_dp, 0.0E0_dp)
  call f(z1, y1)
  call f(z2, y2)

  ux = real((y2 - y1) / (2.0E0_dp * eps))
  vx = aimag((y2 - y1) / (2.0E0_dp * eps))

endsubroutine calcdx


subroutine calcdy(f, z, uy, vy)

  external :: f
  complex(dp), intent(in) :: z
  real(dp), intent(out) :: uy, vy

  complex(dp) :: z1, z2, y1, y2
  real(dp) :: eps = 1.0E-8_dp

  z1 = z - eps * (0.0E0_dp, 1.0E0_dp)
  z2 = z + eps * (0.0E0_dp, 1.0E0_dp)
  call f(z1, y1)
  call f(z2, y2)

  uy = real((y2 - y1) / (2.0E0_dp * eps))
  vy = aimag((y2 - y1) / (2.0E0_dp * eps))

endsubroutine calcdy


end module newton

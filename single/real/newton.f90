module newton

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

contains


subroutine new_real_single(f, x, y)

  external :: f
  real(dp), intent(in) :: x
  real(dp), intent(out) :: y

  real(dp) :: xm, xn
  real(dp) :: fm, dfm, fn
  real(dp) :: eps = 1.0E-3_dp
  real(dp) :: prec = 1.0E-10_dp
  logical :: goon

  goon = .true.
  xm = x
  call f(xm, fm)

  if (isnan(fm)) then
    write(*,*)  'Seed evaluates to NaN'
    call exit
  else if ((1 / fm).eq.0.0E0_dp) then
    write(*,*)  'Seed evaluates to infinity'
    call exit
  endif

  do while (goon)
    call deriv(f, xm, dfm)
    xn = xm - eps * fm / dfm
    call f(xn, fn)
    if ((1 / fn).eq.0.0E0_dp) then
        write(*,*) "Jumped far away and function returned infinity."
        call exit
    endif
    if (abs(fn).lt.prec) then
      goon = .false.
    else
      xm = xn
      fm = fn
    endif
  enddo
  y = xn

end subroutine new_real_single


subroutine deriv(f, x, y)

  external :: f
  real(dp), intent(in) :: x
  real(dp), intent(out) :: y

  real(dp) :: eps = 1.0E-8_dp
  real(dp) :: x1, x2, y1, y2

  x1 = x - eps
  x2 = x + eps
  call f(x1, y1)
  call f(x2, y2)

  y = (y2 - y1) / (2.0E0_dp * eps)

end subroutine deriv


end module newton

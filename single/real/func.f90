module func

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

real(dp) :: a = -1.0E0_dp
real(dp) :: b = -1.0E1_dp

contains


subroutine f(x, y)

  real(dp), intent(in) :: x
  real(dp), intent(out) :: y

  y = x**3 + a*x**2 - b*x + b

end subroutine f


end module func

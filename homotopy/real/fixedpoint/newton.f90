module newton

use gaussjordan, only : eliminate

implicit none


integer, parameter :: dp = selected_real_kind(15,307)

contains


subroutine homotopy_fixed_point(f, N, a, eps, y, raiser)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: a(N)
  real(dp), intent(in) :: eps
  real(dp), intent(out) :: y(N)
  integer, intent(out) :: raiser

  real(dp) :: t
  real(dp) :: x(N)
  integer :: i
  integer :: imax

  raiser = 0
  imax = int(1 / eps)

  y = a
  do i=0,imax
    if (raiser.eq.0) then
      if (i.eq.imax) then
          t = 1.0E0_dp
      else
        t = i * eps
      endif
      x = y
      call new_real_multi(f, N, t, a, x, y, raiser)
      write(*,"(a,F5.3)", advance="no") 't value = ', t
      write(*,"(A)", advance="no") "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
    endif
  enddo

end subroutine homotopy_fixed_point


subroutine new_real_multi(f, N, t, a, x, y, raiser)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: a(N)
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)
  integer, intent(out) :: raiser

  real(dp) :: xm(N), xn(N)
  real(dp) :: fm(N), dfm(N,N), fn(N)
  real(dp) :: step(N)
  real(dp) :: eps = 1.0E-1_dp
  real(dp) :: prec = 1.0E-10_dp
  logical :: goon
  integer :: gooncount
  integer :: i
  integer :: counter

  raiser = 0
  counter = 0

  goon = .true.
  xm = x
  call func(f, N, t, a, xm, fm)

  do i=1,N
    if (isnan(fm(i))) then
      write(*,*)  'Seed evaluates to NaN'
      raiser = 1
      goon = .false.
    else if ((1.0E0_dp / fm(i)).eq.0.0E0_dp) then
      write(*,*)  'Seed evaluates to infinity'
      raiser = 1
      goon = .false.
    endif
  enddo

  do while (goon)
    gooncount = 0
    call calcjacob(f, N, t, a, xm, dfm)
    call eliminate(N, dfm, -fm, step, raiser)
    if (raiser.eq.0) then
      xn = xm + eps * step
      call func(f, N, t, a, xn, fn)
      do i=1,N
        if (isnan(fn(i))) then
          write(*,*) "Bad seed, Gauss-Jordan returned NaNs."
          raiser = 1
        endif
        if ((1 / fn(i)).eq.0.0E0_dp) then
          write(*,*) "Jumped far away and function returned infinity."
          raiser = 1
        endif
        if (abs(fn(i)).lt.prec) then
          gooncount = gooncount + 1
        endif
      enddo
      if (gooncount.eq.N) then
        goon = .false.
      else
        xm = xn
        fm = fn
      endif
      counter = counter + 1
    else
      goon = .false.
    endif
    if (counter.gt.1000000) then
      write(*,*) "Newton method did not converge at intermediate step."
      goon = .false.
      raiser = 1
    endif
  enddo
  y = xn

end subroutine new_real_multi


subroutine func(f, N, t, a, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: a(N)
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: y0(N)

  call f(x, y0)

    y = (1 - t) * (x - a) + t * y0

end subroutine func


subroutine calcjacob(f, N, t, a, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: a(N)
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N,N)

  integer :: i

  do i=1,N
    call deriv(f, N, t, a, i, x, y(:, i))
  enddo

end subroutine calcjacob


subroutine deriv(f, N, t, a, i, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: a(N)
  integer, intent(in) :: i
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: eps = 1.0E-8_dp
  real(dp) :: x1(N), x2(N), y1(N), y2(N)

  x1 = x
  x1(i) = x1(i) - eps
  x2 = x
  x2(i) = x2(i) + eps
  call func(f, N, t, a, x1, y1)
  call func(f, N, t, a, x2, y2)

  y = (y2 - y1) / (2.0E0_dp * eps)

end subroutine deriv


end module newton

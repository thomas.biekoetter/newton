program main

use func, only : f, N
use newton, only : homotopy_fixed_point

implicit none

integer, parameter :: dp = selected_real_kind(15,307)

real(dp), dimension(:), allocatable :: seed ! Initial x-value
real(dp), dimension(:), allocatable :: res ! Root
real(dp), dimension(:), allocatable :: check ! f-value at root

integer :: raiser
real(dp) :: eps = 1E-3_dp

allocate(seed(N))
allocate(res(N))
allocate(check(N))

seed = 1

call homotopy_fixed_point(f, N, seed, eps, res, raiser)
if (raiser.eq.0) then
  call f(res, check)
  write(*,*) "f has root at: ", res
  write(*,*) "f(root) = ", check
else
  write(*,*) "An error occured. No solution found. Try different seed"
endif

end program main

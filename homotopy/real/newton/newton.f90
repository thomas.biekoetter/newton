module newton

use gaussjordan, only : eliminate

implicit none


#ifdef quad
  integer, parameter :: dp = selected_real_kind(30, 4931)
#else
  integer, parameter :: dp = selected_real_kind(15,307)
#endif

contains


subroutine homotopy_newton(f, N, a, eps, y, raiser)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: a(N)
  real(dp), intent(in) :: eps
  real(dp), intent(out) :: y(N)
  integer, intent(out) :: raiser

  real(dp) :: t
  real(dp) :: x(N)
  integer :: i
  integer :: imax
  real(dp) :: yprec(N)
  real(dp) :: ya(N)

  call f(a, ya)

  raiser = 0
  imax = int(1 / eps)

  y = a
  do i=0,imax
    if (raiser.eq.0) then
      if (i.eq.imax) then
          t = 1.0E0_dp
      else
        t = i * eps
      endif
      x = y
      call adjustprecision(f, x, N, yprec)
      call new_real_multi(f, N, t, ya, x, y, raiser, yprec)
      write(*,"(a,F5.3)", advance="no") 't value = ', t
      write(*,"(A)", advance="no") "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"
    endif
  enddo
  x = y
  yprec = 1.E-10_dp
  call new_real_multi(f, N, t, ya, x, y, raiser, yprec)

end subroutine homotopy_newton


subroutine adjustprecision(f, x, N, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  integer :: i

  call f(x, y)
  y = abs(y)
  do i=1,N
    if (y(i).gt.1.0E0_dp) then
      y(i) = y(i) * 1.0E-8_dp
    else
      y(i) = 1.0E-8_dp
    endif
  enddo

end subroutine adjustprecision


subroutine new_real_multi(f, N, t, ya, x, y, raiser, prec)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: ya(N)
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)
  integer, intent(out) :: raiser
  real(dp), intent(in) :: prec(N)

  real(dp) :: xm(N), xn(N)
  real(dp) :: fm(N), dfm(N,N), fn(N)
  real(dp) :: step(N)
  real(dp) :: eps = 1.0E-1_dp
  logical :: goon
  integer :: gooncount
  integer :: i
  integer :: counter
  logical :: stepnonzero

  raiser = 0
  counter = 0

  goon = .true.
  xm = x
  call func(f, N, t, ya, xm, fm)
  call calcjacob(f, N, t, ya, xm, dfm)
  call eliminate(N, dfm, -fm, step, raiser)
  call checkstepnonzero(step, N, stepnonzero)
  if (.not.stepnonzero) then
    xm = 1.01E0_dp * x
    call func(f, N, t, ya, xm, fm)
    call calcjacob(f, N, t, ya, xm, dfm)
    call eliminate(N, dfm, -fm, step, raiser)
  endif

  do i=1,N
    if (isnan(fm(i))) then
      write(*,*)  'Seed evaluates to NaN'
      raiser = 1
      goon = .false.
    else if (abs(1.0E0_dp / fm(i)).lt.1.0E-100_dp) then
      write(*,*)  'Seed evaluates to infinity'
      raiser = 1
      goon = .false.
    endif
  enddo

  do while (goon)
    gooncount = 0
    call calcjacob(f, N, t, ya, xm, dfm)
    call eliminate(N, dfm, -fm, step, raiser)
    if (raiser.eq.0) then
      xn = xm + eps * step
      call func(f, N, t, ya, xn, fn)
      do i=1,N
        if (isnan(fn(i))) then
          write(*,*) "Bad seed, Gauss-Jordan returned NaNs."
          raiser = 1
        endif
        if (abs(1 / fn(i)).lt.1.0E-100_dp) then
          write(*,*) "Jumped far away and function returned infinity."
          raiser = 1
        endif
        if (abs(fn(i)).lt.prec(i)) then
          gooncount = gooncount + 1
        endif
      enddo
      if (gooncount.eq.N) then
        goon = .false.
      else
        xm = xn
        fm = fn
      endif
      counter = counter + 1
    else
      goon = .false.
    endif
    if (counter.gt.1000000) then
      write(*,*) "Newton method did not converge at intermediate step."
      goon = .false.
      raiser = 1
    endif
  enddo
  y = xn

end subroutine new_real_multi


subroutine func(f, N, t, ya, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: ya(N)
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: yx(N)

  call f(x, yx)

  y = yx - sqrt(1 - t) * ya

end subroutine func


subroutine calcjacob(f, N, t, ya, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: ya(N)
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N,N)

  integer :: i

  do i=1,N
    call deriv(f, N, t, ya, i, x, y(:, i))
  enddo

end subroutine calcjacob


subroutine deriv(f, N, t, ya, i, x, y)

  external :: f
  integer, intent(in) :: N
  real(dp), intent(in) :: t
  real(dp), intent(in) :: ya(N)
  integer, intent(in) :: i
  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  real(dp) :: eps
  real(dp) :: x1(N), y1(N)

  eps = 1.0E-8_dp

  ! x1 = x
  ! x1(i) = x1(i) - eps
  ! x2 = x
  ! x2(i) = x2(i) + eps
  ! call func(f, N, t, a, x1, y1)
  ! call func(f, N, t, a, x2, y2)

  ! y = (y2 - y1) / (2.0E0_dp * eps)

  y = 0.0E0_dp
  x1 = x
  x1(i) = x(i) + 2 * eps
  call func(f, N, t, ya, x1, y1)
  y = y - y1
  x1(i) = x(i) + eps
  call func(f, N, t, ya, x1, y1)
  y = y + 8 * y1
  x1(i) = x(i) - eps
  call func(f, N, t, ya, x1, y1)
  y = y - 8 * y1
  x1(i) = x(i) - 2 * eps
  call func(f, N, t, ya, x1, y1)
  y = y + y1
  y = y / (12.0E0_dp * eps)

end subroutine deriv


subroutine checkstepnonzero(s, N, y)

  integer, intent(in) :: N
  real(dp), intent(in) :: s(N)
  logical, intent(out) :: y

  integer :: i

  y = .false.
  do i=1,N
    if (abs(s(i)).gt.1.0E-12_dp) then
      y = .true.
      exit
    endif
  enddo

end subroutine checkstepnonzero


end module newton

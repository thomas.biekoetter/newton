module util

implicit none


#ifdef quad
  integer, parameter :: dp = selected_real_kind(30, 4931)
#else
  integer, parameter :: dp = selected_real_kind(15,307)
#endif

contains


subroutine writematrix(x, n1, n2)

  integer, intent(in) :: n1, n2
  real(dp), intent(in) :: x(n1,n2)

  integer :: i, j

  write(*,*)
  do i=1,n1
    write(*,'(*(1X,e10.3))') (x(i,j), j=1,n2)
  enddo
  write(*,*)

end subroutine writematrix


end module util

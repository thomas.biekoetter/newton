module gaussjordan

implicit none

#ifdef quad
  integer, parameter :: dp = selected_real_kind(30, 4931)
#else
  integer, parameter :: dp = selected_real_kind(15,307)
#endif

contains


subroutine eliminate(N, A, B, y, raiser)

  integer, intent(in) :: N
  real(dp), intent(in) :: A(N,N)
  real(dp), intent(in) :: B(N)
  real(dp), intent(out) :: y(N)
  integer, intent(out) :: raiser

  integer :: i, j, k
  real(dp) :: aug(N,N+1)
  real(dp) :: mult
  real(dp) :: pivot, swap(N+1)
  integer :: ipivot
  logical :: haszerorow

  raiser = 0

  do i=1,N
    do j=1,N
      aug(j,i) = A(j,i)
    enddo
  enddo
  do i=1,N
    aug(i,N+1) = B(i)
  enddo

  do k=1,N
    do i=1,N
      if(i.ne.k) then
        if (raiser.eq.0) then
          call checkzerorow(aug, N, N+1, haszerorow)
          if (haszerorow) then
            write(*,*) 'Ill-defined system, found flat direction.'
            raiser = 1
          endif
          pivot = aug(k,k)
          ipivot = k
          do j=k+1,N
            if (abs(aug(j,k)).gt.abs(pivot)) then
              pivot = aug(j,k)
              ipivot = j
            endif
          enddo
          if (ipivot.ne.k) then
            swap = aug(k,:)
            aug(k,:) = aug(ipivot,:)
            aug(ipivot,:) = swap
          endif
          mult = aug(i,k) / aug(k,k)
          ! write(*,*) aug(i,k), aug(k,k), mult
          do j=1,N+1
            aug(i,j) = aug(i,j) - aug(k,j) * mult
          enddo
        endif
      endif
    enddo
  enddo

  if (raiser.eq.0) then
    do i=1,N
      y(i) = aug(i,N+1) / aug(i,i)
    enddo
  endif

end subroutine eliminate


subroutine checkzerorow(a, n1, n2, y)

  integer, intent(in) :: n1, n2
  real(dp), intent(in) :: a(n1,n2)
  logical, intent(out) :: y

  integer :: i, j
  real(dp) :: numzero = 1.0E-12
  logical, dimension(n1) :: yr

  do i=1,n1
    yr(i) = .true.
    do j=1,n2
      if (abs(a(i,j)).gt.numzero) then
        yr(i) = .false.
      endif
    enddo
  enddo

  y = .false.
  do i=1,n1
    if (yr(i)) then
      y = .true.
    endif
  enddo

end subroutine checkzerorow


end module gaussjordan

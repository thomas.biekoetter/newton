module func

implicit none

#ifdef quad
  integer, parameter :: dp = selected_real_kind(30, 4931)
#else
  integer, parameter :: dp = selected_real_kind(15,307)
#endif

integer, parameter :: N = 6

contains


subroutine f(x, y)

  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  y(1) = x(1) + x(2)**3 + x(4) + 0.0001
  y(2) = x(5)**3 + x(6) - 4
  y(3) = x(1)**3 + x(2) + x(3)**2 + 2*x(5) + x(6)
  y(4) = x(1) - x(2)**3*x(5) - 10000
  y(5) = x(1) - x(3)*x(4) + 0.01
  y(6) = x(5) - x(3)*x(6)**6 - 1

end subroutine f


end module func

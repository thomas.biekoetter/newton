## Contributors

This software was written by
[Thomas Biekötter], and it is without
warranty of any kind.

## License

This software is public under the
GNU GENERAL PUBLIC LICENSE version 3.

[Thomas Biekötter]: mailto:thomas.biekoetter@desy.de

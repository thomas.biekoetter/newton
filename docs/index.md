# newton: Finding roots of non-linear equations

A collection of routines to find roots of
an equation or system of equations in real
or complex variables using the Newton method.

* **single/real/** Contains a method to
find a root of a real function with
a single variable.
* **single/complex/** Contains a method to
find a root of a complex function with
a single variable.
* **multi/real/** Contains a method to
find a root of a system of N real functions with
N variables.
* **multi/complex/** Contains a method to
find a root of a system of N complex functions with
N variables.

The code can be download from the
[git repository].

In order to use `newton`, the user has
to specify the function(s) in the
module `func.f90`, along with the
number of functions/variables N in
case of a multivariate function.
Finally, in the main program (`main.f90`)
the seed has to be set. The program
is compiled and executed with the following
commands from within the corresponding
directory:
```
make all
./rooter
```
If a solution is found, the main program
prints the root to the terminal and evaluates
the function at this point in order to check
the result. If the system of equations is
ill-defined, or when the seed is chosen in
such a way that the algorithm does not converge,
then the program will just exit.

[git repository]: https://gitlab.com/thomas.biekoetter/newton

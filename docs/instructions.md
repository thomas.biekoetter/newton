In case of a single-valued real function,
the user has to set the following object
in the module `func.f90`:
```
subroutine f(x, y)

  real(dp), intent(in) :: x
  real(dp), intent(out) :: y

  y = ...

end subroutine f
```
The only difference for complex functions
is that the input and output variables
have to be defined with the type `complex(dp)`.

In case of a system of equations,
the user has to
define the following two objects
within this module:
```
integer, parameter :: N = ...

subroutine f(x, y)

  real(dp), intent(in) :: x(N)
  real(dp), intent(out) :: y(N)

  y(1) = ...
  ...

end subroutine f
```
Finally, the user has to set
a starting point for the algorithm.
This is done in the respective
main program files `main.f90`.
The relevant object is called `seed`.
For real or complex functions in
one variable this object has
the type `real(dp)`/`complex(dp)`.
For multivariate real/complex
functions the type is
`real(dp), dimension(N)`/
`complex(dp), dimension(N)`.

After the above objects have been
defined, the code can be compiled
and executed by doing:
```
make all
./rooter
```

The files `func.f90` and
`main.f90` in each
directory contain an example
implementation of a function or
system of functions whose zeros
are to be found.

Depending on the form of the
equations there might or might
not be a solution. Whether a
solution is found also strongly
depends on the starting point.
